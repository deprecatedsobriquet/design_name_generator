# License


## Logan Riley CC BY-NC 2.0

Human text: https://creativecommons.org/license/by-nc/2.0
Full text:  https://creativecommons.org/licenses/by-nc/2.0/legalcode

## Main

This work is licensed under the Creative Commons Attribution-NonCommercial 2.0 Generic License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/2.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

