#!/usr/bin/env python

from random import random, randint, choice

import os

"""
Design name generator tool
"""

# CONST

__version__ = '1.2'
__author__ = 'Logan Riley'

NR_NAMES = 5

# init
THINGS = []
MODIFIERS = []

# populate some initial consts
THINGS += ['mist', 'wind', 'storm', 'tempest', 'nimbus']
THINGS += ['sword', 'shield', 'lance', 'javelin', 'spike']
THINGS += ['raptor', 'hawk', 'bird', 'wing', 'swallow', 'stork', 'albatross', 'buzzard', 'vulture', 'eagle']
THINGS += ['gambit', 'puzzle', 'cryptic']
THINGS += ['spirit', 'ghost', 'wraith', 'gremlin']
THINGS += ['eye', 'occulus', 'sense']
THINGS += ['panther', 'cat', 'leopard']
THINGS += ['badger', 'aardvark']
THINGS += ['snake', 'viper']
THINGS += ['potato', 'onion', 'cabbage']
THINGS += ['fast', 'change', 'strike', 'delta']
THINGS += ['ship', 'craft']
THINGS += ['burst']
###

MODIFIERS += ['little', 'big', 'short', 'long', 'vertical', 'wide', 'giant']
MODIFIERS += ['heavy', 'lean', 'horned']
MODIFIERS += ['hot', 'cold', 'frozen', 'burnt', 'ice']
MODIFIERS += ['snowy', 'desert', 'woodland']
MODIFIERS += ['swift', 'strong', 'potent', 'tenacious', 'graceful', 'mad', 'bad']
MODIFIERS += ['sky', 'night']
MODIFIERS += ['blue', 'red', 'green', 'grey', 'orange', 'yellow', 'violet', 'pink']
MODIFIERS += ['quantum', 'lazer', 'quark', 'boson']


###

def get_uniq(lst):
    """ find unique items in list and make lowercase

    
    """

    if type(lst) is not list:
        raise TypeError(" input to get_uniq is not a list")

    if not len(lst) > 0:
        raise ValueError(" input list to get_uniq has no elements")
        
    out = []

    for e in lst:
        e_ = e.lower()
        if e_ not in out:
            out.append(e_)

    return out


def read_words(inp_fn='words.inp'):
    """ grab things and thing modifiers from input file
    """
    
    TNGS = []
    MODS = []

    str_ = ' ... ERROR : inp_fn={} is not valid'.format(inp_fn)
    if not os.path.isfile(inp_fn):
        raise ValueError(str_)
    
    print(' ... reading from input file : {}'.format(inp_fn))

    with open(inp_fn,'r') as f:
        for ln in f.readlines():
            typ, label = ln.rstrip().split()

            if 'thing' in typ.lower():
                TNGS.append(label)
            elif 'mod' in typ.lower():
                MODS.append(label)
            else:
                print(' ... ... WARNING : I do not understand type={}, label={} ... skipping'.format(typ, label))
                pass            

    return TNGS, MODS

            

###
def joiner(m,t):
    return m + t.capitalize()


def gen_name(things=THINGS, mods=MODIFIERS, \
             verbose=True, i_ret_lst=False, \
             **kwargs):
    """ generate dmy design name of form modThing
    """
    
    # clean input
    tngs_u = get_uniq(things)
    mods_u = get_uniq(mods)
    
    nr_tngs = len(tngs_u)
    nr_mods = len(mods_u)

    # choose thing and modifier
    thing = choice(tngs_u)
    modif = choice(mods_u)

    name = joiner(modif, thing)

    # print
    if verbose:
        print(' generated name is {}'.format(name))

    if i_ret_lst:
        t2 = tngs_u.remove(thing)
        m2 = mods_u.remove(modif)
        
        return name, [t2,m2]

    return name



if __name__ == '__main__':
    print('*'*60)
    
    print('> generating {} names'.format(NR_NAMES))
    for n in range(NR_NAMES):
        gen_name()

    print('> testing generation from file')
    T, M = read_words()
    
    for n in range(NR_NAMES):
        gen_name(things=T, mods=M)
